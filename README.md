# Labview IDE Tortoise Git Integration #

This project integrates the TortoiseGit project into the Labview IDE.  It provides Icon Overlays and access to TortoiseGit Commands directly from the Labview IDE.  It is meant to be an open source project provider that the community can contribute too.  None of the vi's have been password protected and can be modified and re-used per the license agreement below.  

This project is currently a beta release.  It was a side project I put together as i was currently using SVN but decided to switch to a distributed version control and GIT is the king of that. As with most project providers this may slow down the Labview IDE when working within very large projects with lots of components. 

If possible please try to track issues or feature requests using the issue tracker associated with this project.  I will also try to keep up with any threads associated with this project within the NI or Lavag forums. 

----------------
# Latest Version:
  - ### [v0.8.1.7](https://bitbucket.org/jed_d/lv_tortoisegit/downloads/lv_tortoisegit-0.8.1.7.vip)

     - Addressed VIPM installation issues for LV2014 (please manually restart labview after installation)
 
-----------------------
## Addon Features
* Tortoise Git Icon Overlays within Labview IDE
* Tortoise Git Commands from within Labview IDE
* Ability to directly reload a project from within the Project Window, as well as prompts to reload the project when performing actions that require a reload.  
* Open Git Bash from project window 

--------------------
## Installation Requirements##
**
Currently this package only supports windows.
I have built the package in Labview 2013 so currently that is the only supported version.  I'm hoping someone else can help me build it for older LV versions.**  

Using this package requires you install these two other major components.  

### [Git for Windows](http://msysgit.github.io/)

   This is the main package for git integration into your windows.  I would recommend installing Git into your Windows Path Environment, it is not required, as it allows you to perform git commands from the command line.  However, this windows release also comes with a version of GIT Bash which can be very helpful.  I do not have much experience with the GUI components of this package as I use the TortoiseGit windows integration.

If you think you might eventually use Git a your primary source control too, I would really recommend setting up SSH keys.  This is not a necessary step at this point but just more information.  This [article](http://blog.assembla.com/assemblablog/tabid/12618/bid/77264/Setting-Up-Git-on-Windows-in-Four-Easy-Steps.aspx) has a brief overview of how to set them up within Windows using Putty and TortoiseGit.

### [TortoiseGit](https://code.google.com/p/tortoisegit/)
   
   This program is a Git version of the popular TortoiseSVN client. It provides icon overlays within windows explorer as well as access to a range of Git Commands.  This project provider allows some of these actions to be initiated from within the Labview IDE.  As with Git for Windows, I would recommend installing TortoiseGit into your path as this make issues some commands easier.  It is not required.  

After installing TortoiseGit I would recommend setting up the Merge/Compare options just as your would with SVN.  You can find the recommended settings at this [NI Knowledge Base](http://digital.ni.com/public.nsf/allkb/1A300A1BB1833EE786257A7C0077B422)

------------------------
## Installation ##
### Installing this package will restart labview. Save any changes to your current work prior to installing.  

This package installs using the JKI VI Package Manager.

After installation you will be presented with a TortoiseGit Setting Screen. In order for the provider to work you must point it to the executables for both GIT and TortoiseGit.  If you have installed these item into your Windows PATH, then they are "git" and "tortoisegitproc" respectively.  If you have not installed them into your path, you will have to browse to the executable location and set the paths there.  You may also provide a location for your Git Bash executable if you have installed that when installing git.  

* For Git this path is typically : C:\Program Files (x86)\Git\cmd\git.exe
* For Tortoisegit this path is typically : C:\Program Files (x86)\TortoiseGit\bin\tortoisegitproc.exe or C:\Program Files\TortoiseGit\bin\tortoisegitproc.exe
* For Git Bash this path is typically: C:\Program Files (x86)\Git\bin\sh.exe

### You must test the paths before saving the settings, otherwise the addon will be disabled.

This settings screen also allows your to disable the addon, set the refresh update time and the reload timer.  

The reload timer is how long the addon waits for a project to reload when you issue a reload command. 

----------------------------------
## Using this Addon
The addon will provide icon overlays for file status and allow you to issue Git commands from within the Project Window.  Unfortunately this is limited to the project provider and is not available when directly viewing a class or library.  

* One key feature of this addon is that it allows your to reload a project directly from the project window.  Right clicking on the project path will provide you with a "Reload Project" option.  This will allow you to manually reload the project if you perform git actions outside of the IDE.  This is required for instance if you change branches and the project file list is different in your new feature branch.  
* The Addon is currently setup such that performing a command on a folder, project files, class or library will perform that same command on all items below that object.
     - For exampling commiting the project file currently adds all the files within the project window to the commit window.  The same applies if committing a virtual folder or library.  
* Due to a limitation of the TortoiseGit command line, this project can only commit up to 500 files at once.  I have tried to handle this with feedback to the user if you commit to large of a subset.  
* **This addon requires that the .git folder is at the same level, a sub-folder of the location of your project file, or that your project file is initially being tracked by a git repo.  If this check does not pass at the beginning the Addon will be disabled for your project
**
* If you revert an individual vi from the project window, this provider will close any open instances of that VI.  


---------------------------------

### Path Forward
This is a beta release however i would like to add the following components to the provider in the future

* Quick Drop actions for GIT commands
* Eventually i would like to perform most commands directly using GIT and move away from Tortoise Git
* Clean up Menus
* Parse options 
* Provide individual commit options for classes, project and libraries.  
* Develop a New Project Creation Provider for pulling a repo into a folder and populating a new project.  
-----------------

## Changelog 
 --v0.8.1.1 Git Bash Integration Added
 --v0.8.0 Initial Release
-------------------------------

## FORK ME

This project is open source and I hope people will have a chance to contribute to the project.  If you fork the project, develop something great and don't mind sharing it, please issue a pull request and we can integrate those changes.  One thing to note is that the Labview IDE portion of this project relies on the Project Provider API that NI has provided.  In most cases this may not be needed to make changes, however if you change any of the interface vi names or certain other files, you will have to resign the provider config file.  If you are interested in this aspect of the project more info on Labview Project Providers is found [here](https://decibel.ni.com/content/groups/labview-project-providers)
_________
## License 

All icons used in this package are used per the Tortoise Icon License.  

Copyright (c) 2014, John Donaldson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the John Donaldson.
4. Neither the name of the John Donaldson nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY John Donaldson ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL John Donaldson BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.